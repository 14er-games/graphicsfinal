
#include "BulletHit.h"

BulletHit::BulletHit(std::string texture, float gravity, glm::vec3 pos) {
    shader = new ParticleShader(texture);
    material = new Material();
    model = new Model();
    this->gravity = gravity;
    this->location = pos;
    age = 0;
    doDestroy = false;
    particleLife = 50;

}


void BulletHit::draw() {
    if (age < 110) {


        // Set the uniforms of the shader
        shader->uniformMat4("modelMatrix", glm::mat4(1.0f));
        shader->uniformMat4("viewMatrix", GameEngine::engineCamera->getViewMatrix());
        shader->uniformMat4("projectionMatrix",
                            GameEngine::engineCamera->getProjectionMatrix(GameEngine::engineWindow));

        // Update particles, deleting as needed
        for (long unsigned int i = 0; i < particles.size(); i++) {
            Particle p = particles.at(i);
            if (p.age > p.lifespan) {
                particles.erase(particles.begin() + i);
                continue;
            }
            p.x += p.dx;
            p.dy -= gravity;
            if (p.dy < -0.1f)
                p.dy = -0.1f;
            p.y += p.dy;
            p.z += p.dz;
            p.age++;
            p.size = 1 - (p.age / p.lifespan); // Shrink with age
            particles.at(i) = p;
        }

        // Draw all particles
        shader->useProgram();
        for (Particle p : particles) {
            shader->drawParticle(p.x, p.y, p.z, p.size);
        }

        age++;

        for (int i = 0; i < lights.size(); i++)
        {
            Light* x = lights.at(i);
            x->intensity -= 0.01;
            if (x->counter <=0){
                x->setDestroy(true);
                lights.erase(lights.begin() + i);
                --i;
            }else{
                --x->counter;
            }

        }

        if (age > 20)
            if (particleLife - age > 0)
                if (particles.size() < 200)
                    addParticle(location.x, location.y, location.z,
                                0.1 * getRand(), 0.1 * getRand(), 0.1 * getRand(), 120);


    }
}
void BulletHit::addParticle(float x, float y, float z, float dx, float dy, float dz, float lifespan) {
    // Add particle to the system
    if (lifespan < 1)
        return;

    particles.push_back({x, y, z, dx, dy, dz, 0.05f, 0.0f, lifespan});
}

int BulletHit::size() {
    return particles.size();
}

bool BulletHit::shouldDestroy() {
    return doDestroy;
}

void BulletHit::addLight(GameEngine *engine) {
    Light* addedLight = new Light(LIGHT_TYPE::POINT, 1.0f, 1.0f, 1.0f, 2.0f, location.x, location.y, location.z);
    addedLight->counter = 80;
    engine->addLight(addedLight, false);
    //Duration that light should last
    lights.push_back(addedLight);
}