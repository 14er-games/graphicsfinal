//
// Created by isabe on 12/6/2020.
//

#ifndef GRAPHICSFINAL_ENEMY_H
#define GRAPHICSFINAL_ENEMY_H


#include "engine/objects/ParticleSystem.h"
#include "FirstPersonCamera.h"
#include "engine/Object.h"
#include "engine/shaders/ColorGouradShader.h"
#include "engine/materials/MatteSkyBlue.h"
#include "engine/models/ColoredCube.h"
#include "engine/GameEngine.h"
#include "engine/Collider.h"
#include "engine/objects/Cube.h"
#include "engine/materials/MetallicOrange.h"
#include "PlayerModel.h"

class Enemy : public CubeObject {
public:
    Enemy(Object* player);

    ~Enemy();

    void draw() override;

private:
    Object* saveObj;
};
#endif //GRAPHICSFINAL_ENEMY_H
