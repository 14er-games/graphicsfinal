#include "Weapon.h"

Weapon::Weapon(Object* person, Camera* cam) {
    shaderProgram = new ColorGouradShader();
    //may or may not use later
    //this->person = person;
    this->cam = cam;

    // Load material
    material = new WeaponMaterial();
    timer = 0;
    increment = 1;
    setCollider(new Collider(this), false);
    //no recoil or scope at the start
    activeRecoil = false;
    activeScope = false;
}

Weapon::~Weapon() {
    delete shaderProgram;
}

void Weapon::draw() {
    //use the shader program
    shaderProgram->useProgram();

    GameEngine::engineShaderProgram = shaderProgram;
    CSCI441::setVertexAttributeLocations(shaderProgram->getAttributeLocation("vertexPosition"),   // vertex position location
                                         shaderProgram->getAttributeLocation("vertexNormal"),     //vertex normal location
                                         -1);// shaderProgram->getAttributeLocation("vertexTexture"));         //vertex texture location
    // Buffer mvp uniforms
    shaderProgram->uniformMat4("modelMatrix", getModelMatrix());
    shaderProgram->uniformMat4("viewMatrix", GameEngine::engineCamera->getViewMatrix());
    shaderProgram->uniformMat4("projectionMatrix", GameEngine::engineCamera->getProjectionMatrix(GameEngine::engineWindow));

    // Buffer material uniforms
    shaderProgram->uniformVec3("materialColor", glm::vec3(material->r, material->g, material->b));
    shaderProgram->uniformFloat("materialDiffuse", material->diffuse);
    shaderProgram->uniformFloat("materialSpecularity", material->specular);
    // Buffer viewing vector uniform
    shaderProgram->uniformVec3("viewingVector", GameEngine::engineCamera->getViewingVector());


    material->use();
    //using CSCI441 library to draw a model of a weapon, will be developed more later
    //want to sync the weapon to where the camera is looking as it is an fps.
    glm::mat4 modelMtx = getModelMatrix();
    glm::mat4 viewMtx = cam->getViewMatrix();

    //if there is an active recoil, we have to change the gun's rotation and position
    float recoilAmount;
    if(activeRecoil){
        recoilAmount = recoil();
    }

    //all necessary transformations for fluid motion inside the game
    if(activeRecoil && activeScope) {
        modelMtx = glm::translate(modelMtx, glm::vec3(0, 10 - recoilAmount, -2 - recoilAmount));
        modelMtx = glm::rotate(modelMtx, 1.57079632679f, glm::vec3(1, 0, 0));
    }
    else if(activeScope){
        modelMtx = glm::translate(modelMtx, glm::vec3(0, 10,-2 ));
        modelMtx = glm::rotate(modelMtx, 1.57079632679f, glm::vec3( 1, 0, 0));
    }

    else if(activeRecoil){
        modelMtx = glm::translate(modelMtx, glm::vec3(1, 10.5 - 4* recoilAmount,2 - 4*recoilAmount));
        modelMtx = glm::rotate(modelMtx, 1.57079632679f + 2 * recoilAmount, glm::vec3( 1, 0, 0));
        modelMtx = glm::rotate(modelMtx, .09817477042f, glm::vec3( 0, 0, 1));
        modelMtx = glm::rotate(modelMtx, 0.0654498469f, glm::vec3( 1, 0, 0));
    }
    else{
        modelMtx = glm::translate(modelMtx, glm::vec3(1, 10.5,2));
        modelMtx = glm::rotate(modelMtx, 1.57079632679f, glm::vec3( 1, 0, 0));
        modelMtx = glm::rotate(modelMtx, .09817477042f, glm::vec3( 0, 0, 1));
        modelMtx = glm::rotate(modelMtx, 0.0654498469f, glm::vec3( 1, 0, 0));
    }
    //model matrix final calculation being multiplied by inverse of view matrix
    modelMtx =   inverse(viewMtx) * modelMtx;

    //scope of the gun drawing.
    glUniformMatrix4fv(shaderProgram->getUniformLocation("modelMatrix"), 1, GL_FALSE, &modelMtx[0][0]);
    glm::mat3 normalMtx = glm::mat3( glm::transpose( glm::inverse(modelMtx) ) );
    glUniformMatrix3fv(shaderProgram->getUniformLocation("normalMatrix"), 1, GL_FALSE, &normalMtx[0][0]);
    CSCI441::drawSolidCylinder(.25, .25, 2.5, 50, 100);


    //body of the gun drawing
    modelMtx = glm::translate(modelMtx, glm::vec3( 0, -2, -.85 ));
    glUniformMatrix4fv(shaderProgram->getUniformLocation("modelMatrix"), 1, GL_FALSE, &modelMtx[0][0]);
    normalMtx = glm::mat3( glm::transpose( glm::inverse(modelMtx) ) );
    glUniformMatrix3fv(shaderProgram->getUniformLocation("normalMatrix"), 1, GL_FALSE, &normalMtx[0][0]);
    CSCI441::drawSolidCylinder(.6, .6, 5, 50, 10);

    //barrel of the gun drawing
    modelMtx = glm::translate(modelMtx, glm::vec3(0 , 0, .38 ));
    glUniformMatrix4fv(shaderProgram->getUniformLocation("modelMatrix"), 1, GL_FALSE, &modelMtx[0][0]);
    normalMtx = glm::mat3( glm::transpose( glm::inverse(modelMtx) ) );
    glUniformMatrix3fv(shaderProgram->getUniformLocation("normalMatrix"), 1, GL_FALSE, &normalMtx[0][0]);
    CSCI441::drawSolidCylinder(.2, .2, 10, 50, 10);

    //left arm
    glm::mat4 lmodelMtx = glm::translate(modelMtx, glm::vec3(-1.5,2,-1.0));
    lmodelMtx = glm::rotate(lmodelMtx, 1.57f, glm::vec3(1.0,0.0,0.1));
    lmodelMtx = glm::rotate(lmodelMtx, -0.4f, glm::vec3(0.0,1.0,0.0));
    lmodelMtx = glm::scale(lmodelMtx, glm::vec3(0.5,1,6));
    glUniformMatrix4fv(shaderProgram->getUniformLocation("modelMatrix"), 1, GL_FALSE, &lmodelMtx[0][0]);
    normalMtx = glm::mat3( glm::transpose( glm::inverse(lmodelMtx) ) );
    glUniformMatrix3fv(shaderProgram->getUniformLocation("normalMatrix"), 1, GL_FALSE, &normalMtx[0][0]);
    CSCI441::drawSolidCube(1);

    //right arm
    modelMtx = glm::translate(modelMtx, glm::vec3(-0.4,2,-1.0));
    modelMtx = glm::rotate(modelMtx, 1.57f, glm::vec3(1.0,0.0,0.0));
    modelMtx = glm::scale(modelMtx, glm::vec3(1,1,6));
    glUniformMatrix4fv(shaderProgram->getUniformLocation("modelMatrix"), 1, GL_FALSE, &modelMtx[0][0]);
    normalMtx = glm::mat3( glm::transpose( glm::inverse(modelMtx) ) );
    glUniformMatrix3fv(shaderProgram->getUniformLocation("normalMatrix"), 1, GL_FALSE, &normalMtx[0][0]);
    CSCI441::drawSolidCube(1);
}

//recoil function. Takes a total of 60 iterations to complete. timer goes to 30 then comes back to 0. Synced with Particle spawn
float Weapon::recoil() {
    timer+=increment;
    if(timer == 30){
        increment = -1;
    }
    else if(timer == 0){
        timer = 0;
        increment = 1;
        activeRecoil = false;
    }
    //returns an angle that the gun will ever so slightly rotate on.
    return (timer / 30) * (M_PI / 64);
}

//sets recoil in the main function
void Weapon::setRecoil() {
    activeRecoil = true;
}

//scope function that player controls
void Weapon::setScope(bool activeScope) {
    this->activeScope = activeScope;

}

