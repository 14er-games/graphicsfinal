
#include "Powerup.h"

Powerup::Powerup() {
	delete material;
	material = new MetallicOrange();

	this->getCollider()->isTrigger = true;
	this->getCollider()->setSize(5, 5, 5);
	this->addTag("powerup");
}

Powerup::~Powerup () {

}
