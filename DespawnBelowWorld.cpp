
#include "DespawnBelowWorld.h"

#include <cstdio>

DespawnBelowWorld::DespawnBelowWorld(Object* check) {
	obj = check;
}

void** DespawnBelowWorld::run(void** args) {
	if (obj->position.y < -70.0f) {
		obj->setDestroy(true);
	}

	return (void**) 0;
}
