#include <map>
#ifndef GRAPHICSFINAL_MUZZLEFLASH_H
#define GRAPHICSFINAL_MUZZLEFLASH_H

#include "engine/objects/ParticleSystem.h"
#include "FirstPersonCamera.h"

class MuzzleFlash : public ParticleSystem{
public:
    MuzzleFlash(std::string texture, float gravity);
    void addParticle(float lifespan, GameEngine *engine);
    void draw() override;
    void setFollow(FirstPersonCamera *cam);
    void addLight(GameEngine* engine, glm::vec3 location);
    int size();
private:
    FirstPersonCamera *cam;
    std::vector<Light*> lights;
    std::vector<Particle> flashes;
    ParticleShader* shader;
    float gravity;
};

#endif //GRAPHICSFINAL_MUZZLEFLASH_H
