
#ifndef POWERUP_OBJECT_CLASS
#define POWERUP_OBJECT_CLASS

#include "engine/objects/Cube.h"
#include "engine/materials/MetallicOrange.h"

class Powerup : public CubeObject {
public:
	Powerup();
	~Powerup();
};

#endif
