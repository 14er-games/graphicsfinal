
#include "engine/Object.h"
#include "engine/shaders/ColorGouradShader.h"
#include "WeaponMaterial.h"
#include "engine/GameEngine.h"
#include "FirstPersonCamera.h"
#include "engine/objects/Cube.h"
#include <CSCI441/objects.hpp>          // draws 3D objects

#ifndef GRAPHICSFINAL_WEAPON_H
#define GRAPHICSFINAL_WEAPON_H
class Weapon : public Object {
public:
    Weapon(Object*, Camera*);
    ~Weapon();
    void draw();
    void setRecoil();
    void setScope(bool);
private:
    float recoil();
    Camera* cam;
    float timer;
    int increment;
    bool activeRecoil;
    bool activeScope;
};
#endif //GRAPHICSFINAL_WEAPON_H