
#include <glm/glm.hpp>
#include <vector>
#include <string>

#include <cstdio>

#include "engine/GameEngine.h"
#include "engine/math/Quaternion.h"
#include "engine/objects/Cube.h"
#include "engine/objects/TexturedPlane.h"
#include "engine/objects/ParticleSystem.h"
#include "engine/scripts/SimplePhysics.h"
#include "engine/scripts/CloseOnEscape.h"
#include "engine/shaders/SkyboxShader.h"
#include "engine/scripts/SimpleMovement.h"

#include "Weapon.h"
#include "FirstPersonCamera.h"
#include "TopDownCamera.h"
#include "MuzzleFlash.h"
#include "SecondShader.h"
#include "Enemy.h"
#include "SpinningParticleShader.h"
#include "PlayerModel.h"
#include "Powerup.h"
#include "DespawnBelowWorld.h"
#include "PlayerPostScript.h"
#include "BulletHit.h"

// Get rand [-1..1]
double getRand() { return (2.0*(rand() / (double)RAND_MAX)) - 1; }

int main () {
    int playerKills = 0;
    int comboCounter = 0;
    double playerShots = 0;
    int particleCount = 45;
    int maxCombo = 0;
	// Print controls
	printf("WASD - Move\n");
	printf("Space - Jump\n");
	printf("Mouse - Turn\n");
	printf("Left Click - Attack\n");

	// Intensity of the color flash tracker
	int* flashIntensity = new int;
	*flashIntensity = 0;

	std::vector<BulletHit*> hits;

	// Create a new game engine and full screen the window
	GameEngine *engine = new GameEngine();
	engine->setWindowSize(-1, -1);
	Light *mainDirectional = new Light(LIGHT_TYPE::DIRECTIONAL, 1.0f, 1.0f, 1.0f, 0.8f, 1.0f, 1.0f, -1.0f);
	engine->addLight(mainDirectional, false);
	engine->addLight(new Light(LIGHT_TYPE::AMBIENT, 1.0f, 1.0f, 1.0f, 0.05f), false);

    //create and set the FP cam
	FirstPersonCamera *FPcam = new FirstPersonCamera();
	engine->setCamera(FPcam, true);

	// Add a skybox
	std::vector<std::string> skyTextures = {"textures/skybox/px.png",
			"textures/skybox/nx.png", "textures/skybox/py.png",
			"textures/skybox/ny.png", "textures/skybox/pz.png",
			"textures/skybox/nz.png"};
	SkyboxShader* skybox = new SkyboxShader(skyTextures);
	engine->setSkyboxShader(skybox, true);

	// Make the window close when we press escape
	CloseOnEscape* closeScript = new CloseOnEscape();
	engine->addScript(closeScript);

	// This can be used to rotate the cube
	Quaternion *rotateCube = new Quaternion();
	rotateCube->euler(0.01f, 1, 2, 1);

	// Add 3 cube objects
	// CubeObject *cube = new CubeObject();
	// cube->translate(glm::vec3(0.0f, 20.0f, 0.0f));
	Script* physics = new SimplePhysics();
	// cube->setPhysicsTick(physics, true);
	// engine->addObject(cube);

	// CubeObject *cube2 = new CubeObject();
	// cube2->translate(glm::vec3(0.0f, 20.0f, -5.0f));
	// cube2->setPhysicsTick(physics, true);
	// engine->addObject(cube2);

	// CubeObject *cube3 = new CubeObject();
	// cube3->translate(glm::vec3(0.0f, 20.0f, 5.0f));
	// cube3->setPhysicsTick(physics, true);
	// engine->addObject(cube3);

    SimpleMovement *movementScript = new SimpleMovement();
    //Add player and attach camera and necessary scripts
    PlayerModel* player = new PlayerModel();
    player->setCam(FPcam);
    player->setPhysicsTick(physics, true);
    player->setControlTick(movementScript, true);
    player->translate(glm::vec3(4.0f,4.0f,3.0f));
    Script* playerPost = new PlayerPostScript(flashIntensity);
    player->setPostTick(playerPost, true);
    FPcam->setFollow(player, true);
    engine->addObject(player);

    TopDownCamera *TDcam = new TopDownCamera(player);

	Weapon *weapon = new Weapon(player, FPcam);
	weapon->translate(glm::vec3(0.0f, 10.0f, 0.0f));
	weapon->setCollider(nullptr, true);
    engine->addObject(weapon);

	// Create a particle system
	ParticleSystem* particles = new ParticleSystem("textures/particle.png", 0.0f);
	SpinningParticleShader* spinningParticleShader = new SpinningParticleShader("textures/particle.png");
	particles->setParticleShader(spinningParticleShader, false);
	engine->addObject(particles);

	MuzzleFlash *muzzleFlash = new MuzzleFlash("textures/flash.png", 0.0f);
	engine->addObject(muzzleFlash);
	muzzleFlash->setFollow(FPcam);


	// Turn on 2 pass rendering
	SecondPassShader* secondPass = new SecondShader(1920, 1080);
	engine->setSecondPass(secondPass, true);
    secondPass->uniformFloat("colorDistortIntensity", 0.0f);

	// Add a ground
	TexturedPlaneObject *ground = new TexturedPlaneObject("textures/ground.png");
	ground->translate(glm::vec3(0.0f, -3.0f, 0.0f));
	ground->scale(glm::vec3(200.0f, 1.0f, 200.0f));
	Collider *groundCollide = new Collider(ground, 200,1,200);
	ground->setCollider(groundCollide, false);

	Quaternion* groundRot = new Quaternion();
	groundRot->euler(-0.5f, 1, 0, 0);
	ground->rotation->hamilton(groundRot);
	ground->getCollider()->trackParentRotation = true;
	ground->getCollider()->recalcTransforms();
	delete groundRot;

	engine->addObject(ground);

	//Temp frame counter so that you cannot spam the flash particle system
	int count = 0;
	// Render
	while (engine->render()) {
	    engine->engineCamera->getProjectionMatrix(GameEngine::engineWindow);
	    //Switch to topdown cam for viewport
	    engine->setCamera(TDcam, false);
	    TDcam->update();

        engine->engineCamera->getProjectionMatrix(GameEngine::engineWindow);
        //Draw scissored viewport in top corner
        int windowWidth, windowHeight;
        glfwGetFramebufferSize(GameEngine::engineWindow,&windowWidth,&windowHeight);
        TDcam->setScissoredSize((windowWidth/4), (windowHeight/4));
        glEnable(GL_SCISSOR_TEST);
        glScissor(windowWidth - (windowWidth/4), windowHeight - (windowHeight/4), windowWidth+200, windowHeight);
        glViewport(windowWidth - (windowWidth/4), windowHeight - (windowHeight/4), windowWidth+200, windowHeight);

        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
        engine->renderNoUpdate();
        glDisable(GL_SCISSOR_TEST);
		engine->setCamera(FPcam, false);

        secondPass->uniformInt("frameNumber", GameEngine::engineTimer);
        spinningParticleShader->uniformInt("frameNumber", GameEngine::engineTimer);

        // RAINBOWS!!!
    	secondPass->uniformFloat("colorDistortIntensity", (float) *flashIntensity / 1200.0f);
    	if (*flashIntensity > 0) {
    		--*flashIntensity;
    	}

    	// Spawn "power ups"
    	if (GameEngine::engineTimer % 5 == 0) {
    		Powerup* powerup = new Powerup();
    		Script* physicsScript = new SimplePhysics();
    		powerup->setPhysicsTick(physicsScript, true);
    		Script* depsawnBelowWorld = new DespawnBelowWorld(powerup);
    		powerup->setPostTick(depsawnBelowWorld, true);
    		powerup->translate(glm::vec3(100 * getRand(), 50.0f, 100 * getRand()));
    		engine->addObject(powerup);
    	}
        if (GameEngine::engineTimer % 150 == 0) {
            Enemy* enemy = new Enemy(player);
            Script* depsawnBelowWorld = new DespawnBelowWorld(enemy);
            enemy->setPostTick(depsawnBelowWorld, true);
            enemy->translate(glm::vec3(100 * getRand(), 10.0f, 100 * getRand()));
            engine->addObject(enemy);
        }

        //Decrease intensity of main light as game progresses
        if (GameEngine::engineTimer % 15 == 0) {
            if (mainDirectional->intensity > 0.1)
                mainDirectional->intensity -=0.001;
        }

		// Apply the rotation to the cube
		// cube->rotation->hamilton(rotateCube);
		// cube2->rotation->hamilton(rotateCube);
		// cube3->rotation->hamilton(rotateCube);

		// Spawn particles
		if (particles->size() < 200)
			if (getRand() < 0)
				particles->addParticle(100 * getRand(), 25 * getRand(), 100 * getRand(),
					0.05 * getRand(), 0.05 * getRand(), 0.05 * getRand(), 200);
			//Add frame count check to click so there is a delay between fires
			//Will probably find a more sophistcated way to handle this
			//Easter Egg
		if(comboCounter >= 15){
		    particleCount = 10;
		}
		if(comboCounter > maxCombo){
		    maxCombo = comboCounter;
		}
        if ( count > particleCount && InputSystem::isMouseButtonPressed(0)){
            playerShots += 1;
            count = 0;
            muzzleFlash->addParticle(70, engine);
            //Start the recoil at the same time a particle is spawned
            weapon->setRecoil();
            // Raycast against the scene for enemies, destroying if it
            glm::vec3 origin = FPcam->pos;
            glm::vec3 direction = FPcam->getViewingVector() * -1.0f;
            Object* hit = engine->raycast(origin, direction, 500, 500, player->getCollider(), true);
            if (hit != nullptr) {
            	if (hit->hasTag("enemy")) {
                    BulletHit* hitEnemy = new BulletHit("textures/spark.png", 0.0, hit->position);
                    engine->addObject(hitEnemy);
                    hitEnemy->addLight(engine);
                    hits.push_back(hitEnemy);

            		hit->setDestroy(true);
            		comboCounter++;
            		playerKills++;
            	}
            	else{
            	    comboCounter = 0;
            	}
            }
        }
        count ++;
        //set scope to if the right mouse button is pressed like it is in most fp shooters
        if(InputSystem::isMouseButtonPressed(GLFW_MOUSE_BUTTON_RIGHT)){
            weapon->setScope(true);
        }
        else{
            weapon->setScope(false);
        }

		// if (engine->raycast(glm::vec3(3, 8, 3), glm::vec3(0, -1, 0), 20, 0.01, nullptr, false) != nullptr) {
		// 	// This raycast hit a collisder
		// }

		//Reset to main cam for main window
		engine->postRender();
	}

	// Clean up the engine
	engine->purgeObjects();
	delete engine;

	// Clean up the other stuff
	delete secondPass;
	delete skybox;
	delete rotateCube;
	delete closeScript;
	delete spinningParticleShader;
	delete flashIntensity;

    printf("Your score was: %i!\n", playerKills * 100);
    printf("You killed %i enemies!\n", playerKills);
    double killRate = static_cast<double>(playerKills/playerShots);
    if (playerShots == 0) {
    	killRate = 0;
    }
    printf("You had an accuracy of %.2f%%!\n", 100.0 * killRate);
    printf("Your longest streak without missing was: %i! Try hitting 15 for something special!\n", maxCombo);
	// Return successfully
	return 0;
}
