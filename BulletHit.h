#include "engine/objects/ParticleSystem.h"
// #include <map>
#ifndef GRAPHICSFINAL_BULLETHIT_H
#define GRAPHICSFINAL_BULLETHIT_H
class BulletHit : public ParticleSystem {
public:
    BulletHit(std::string texture, float gravity, glm::vec3 pos);
    void draw() override;
    bool shouldDestroy();
    void addParticle(float x, float y, float z, float dx, float dy, float dz, float lifespan);
    int size();
    double getRand() { return (2.0*(rand() / (double)RAND_MAX)) - 1; }
    void addLight(GameEngine* engine);
private:
    int age;
    bool doDestroy;
    std::vector<Particle> particles;
    glm::vec3 location;
    int particleLife;
    std::vector<Light*> lights;

};

#endif //GRAPHICSFINAL_BULLETHIT_H
