
#include "WeaponMaterial.h"
WeaponMaterial::WeaponMaterial() {
    r = .2617f;
    g = .2800f;
    b = .300f;
    diffuse = .5f;
    specular = 1.0f;
}