
#ifndef GRAPHICSFINAL_PLAYERMODEL_H
#define GRAPHICSFINAL_PLAYERMODEL_H

#include "engine/Object.h"
#include "engine/GameEngine.h"

class PlayerModel : public Object {
public:
    PlayerModel();
    void drawBody(glm::mat4 modelMtx);
    void drawHead(glm::mat4 modelMtx);
    void drawLegs(glm::mat4 modelMtx);
    void draw() override;
    void setCam(Camera* camera);
private:
    float rotateAngle;
    glm::vec3 lastLocation;
    float camRotation = 0.0f;
    glm::vec3 currentHeading;
    Camera* cam;
};
#endif //GRAPHICSFINAL_PLAYERMODEL_H
