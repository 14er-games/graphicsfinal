
#ifndef PLAYER_POST_SCRIPT
#define PLAYER_POST_SCRIPT

#include "engine/Script.h"

class PlayerPostScript : public Script {
public:
	PlayerPostScript(int* powerupTimer);
	~PlayerPostScript();


	void** run(void** args);
private:
	int* timerVar;
};

#endif
