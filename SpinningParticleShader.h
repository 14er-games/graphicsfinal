
#ifndef SPINNING_PARTICLE_SHADER
#define SPINNING_PARTICLE_SHADER

#include <string>
#include <GL/glew.h>

#include "engine/shaders/ParticleShader.h"
#include "engine/TextureUtils.h"

// A billboarding particle shader with transparency support
class SpinningParticleShader : public ParticleShader {
public:
	SpinningParticleShader(std::string texture);
	~SpinningParticleShader();

	void enableAttribs();

	void drawParticle(float x, float y, float z, float size);

	static const char* vertexProgram;
	static const char* geometryProgram;
	static const char* fragmentProgram;
};

#endif
