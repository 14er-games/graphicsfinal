//
// Created by isabe on 12/6/2020.
//

#include "Enemy.h"
double getRands() { return (2.0*(rand() / (double)RAND_MAX)) - 1; }
Enemy::Enemy(Object* player)  {
    delete material;
    material = new MatteSkyBlue();
    saveObj = player;
    this->getCollider()->isTrigger = true;
    this->getCollider()->setSize(1.9, 1.9, 1.9);
    this->addTag("enemy");
}
Enemy::~Enemy(){}

void Enemy::draw() {

    shaderProgram->useProgram();

    GameEngine::engineShaderProgram = shaderProgram;

    // Buffer mvp uniforms
    shaderProgram->uniformMat4("modelMatrix", getModelMatrix());
    shaderProgram->uniformMat4("viewMatrix", GameEngine::engineCamera->getViewMatrix());
    shaderProgram->uniformMat4("projectionMatrix", GameEngine::engineCamera->getProjectionMatrix(GameEngine::engineWindow));

    // Buffer material uniforms
    shaderProgram->uniformVec3("materialColor", glm::vec3(material->r, material->g, material->b));
    shaderProgram->uniformFloat("materialDiffuse", material->diffuse);
    shaderProgram->uniformFloat("materialSpecularity", material->specular);

    // Buffer viewing vector uniform
    shaderProgram->uniformVec3("viewingVector", GameEngine::engineCamera->getViewingVector());

    material->use();
//glm::vec3 newPos(savePos.x*.3 , savePos.y*.8, savePos.z*.3) - this->position));
    float xDistance = saveObj->position.x- this->position.x;
    float yDistance = saveObj->position.y- this->position.y;
    float zDistance = saveObj->position.z- this->position.z;
    glm::vec3 dist = glm::vec3(xDistance,yDistance,zDistance);
    glm::vec3 normal = glm::normalize(dist);

   // float timeFactor = getRands()* 0.02;


    this->translate(normal * 0.06f);

    // Draw model
    model->draw();

}


