
#include "FirstPersonCamera.h"

#include <cstdio>

FirstPersonCamera::FirstPersonCamera() {
    glm::vec3 startingPosition = glm::vec3(5.0f,0.0f,0.0);
    this->cameraSensitivity = 0.005f;
    this->pos = startingPosition;
    this->lookAt = startingPosition + glm::vec3(0.0,1.0,0.0);
    this->up = glm::vec3(0.0f, 1.0f, 0.0f);
    this->follow = nullptr;

    this->horizontalAng = 0.0f;
    this->verticalAng = 1.57f;
}

void FirstPersonCamera::update() {
    glm::vec2 cameraUpdateAmount = glm::vec2(0, 0);
    cameraUpdateAmount = InputSystem::getMouseAcceleration() * cameraSensitivity;
    horizontalAng += cameraUpdateAmount.x;
    verticalAng -= cameraUpdateAmount.y;
    if (verticalAng > M_PI){
        verticalAng = M_PI - 0.001f;
    }else if (verticalAng < 0){
        verticalAng = 0 + 0.001f;
    }
    //hardcoded adjustment values
    if (this->follow != nullptr){
        this->pos = this->follow->position + glm::vec3(0.0,10,-2);
    }
    lookAt = this->pos;
    lookAt.x += sinf(horizontalAng)*sinf(verticalAng);
    lookAt.y += -cosf(verticalAng);
    lookAt.z += -cosf(horizontalAng) * sinf(verticalAng);
}

void FirstPersonCamera::setFollow(Object *obj, bool destroyOld) {
    if (destroyOld && this->follow != nullptr)
        delete follow;
    this->follow = obj;
}


glm::vec3 FirstPersonCamera::getFlashLocation() {
    return glm::normalize(this->lookAt - this->pos);
}

glm::vec3 FirstPersonCamera::getLookAtLoc() {
    return this->lookAt;
}
