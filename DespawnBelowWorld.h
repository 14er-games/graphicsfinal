
#ifndef DESPAWN_BELOW_WORLD_SCRIPT
#define DESPAWN_BELOW_WORLD_SCRIPT

#include "engine/Script.h"
#include "engine/Object.h"

class DespawnBelowWorld : public Script {
public:
	DespawnBelowWorld(Object* check);

	void** run(void** args);

private:
	Object* obj;
};

#endif
