
#include "engine/Camera.h"
#include "engine/GameEngine.h"
#ifndef GRAPHICSFINAL_FIRSTPERSONCAMERA_H
#define GRAPHICSFINAL_FIRSTPERSONCAMERA_H

class FirstPersonCamera : public Camera {
public:
    FirstPersonCamera();
    void update() override;
    void setFollow(Object* obj, bool destroyOld);
    glm::vec3 getLookAtLoc();
    glm::vec3 getFlashLocation();
private:
    bool shouldRotate;
    float cameraSensitivity, horizontalAng, verticalAng;
};


#endif //GRAPHICSFINAL_FIRSTPERSONCAMERA_H
