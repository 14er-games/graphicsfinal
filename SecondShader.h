
#ifndef SECOND_SHADER
#define SECOND_SHADER

#include <GL/glew.h>

#include "engine/ShaderProgram.h"
#include "engine/shaders/SecondPassShader.h"

// A passthrough second pass shader, able to be extended to implement other functionalities on a second pass
class SecondShader : public SecondPassShader {
public:
	SecondShader(int fboWidth, int fboHeight);
	~SecondShader();
	
	static const char* vertexProgram;
	static const char* fragmentProgram;
};

#include "engine/GameEngine.h"

#endif
