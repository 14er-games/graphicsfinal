
#include "MuzzleFlash.h"
#include "FirstPersonCamera.h"

MuzzleFlash::MuzzleFlash(std::string texture, float gravity) : ParticleSystem(texture, gravity) {
    shader = new ParticleShader(texture);
    material = new Material();
    model = new Model();
    this->gravity = gravity;
}

void MuzzleFlash::setFollow(FirstPersonCamera *cam) {
    this->cam = cam;
}

void MuzzleFlash::draw() {
    // Set the uniforms of the shader
    shader->uniformMat4("modelMatrix", glm::mat4(1.0f));
    shader->uniformMat4("viewMatrix", GameEngine::engineCamera->getViewMatrix());
    shader->uniformMat4("projectionMatrix", GameEngine::engineCamera->getProjectionMatrix(GameEngine::engineWindow));

    // Update particles, deleting as needed
    for (long unsigned int i = 0; i < flashes.size(); i++) {
        Particle p = flashes.at(i);
        if (p.age > p.lifespan) {
            flashes.erase(flashes.begin() + i);
            continue;
        }
        p.x += p.dx;
        p.dy -= gravity;
        if (p.dy < -0.1f)
            p.dy = -0.1f;
        p.y += p.dy;
        p.z += p.dz;
        p.age++;
        p.size = 1 - (p.age / p.lifespan); // Shrink with age
        flashes.at(i) = p;
    }

    for (int i = 0; i < lights.size(); i++)
    {
        Light* x = lights.at(i);
        x->intensity -= 0.1;
        if (x->counter <=0){
            x->setDestroy(true);
            lights.erase(lights.begin() + i);
            --i;
        }else{
            --x->counter;
        }

    }

    // Draw all particles
    shader->useProgram();
    for (Particle p : flashes) {
        shader->drawParticle(p.x, p.y, p.z, p.size);
    }
}

void MuzzleFlash::addParticle( float lifespan, GameEngine* engine) {
    glm::vec3 flashLoc = 10.0f * this->cam->getFlashLocation();
    this->flashes.push_back({cam->lookAt.x + flashLoc.x,cam->lookAt.y + flashLoc.y,cam->lookAt.z + flashLoc.z, 0.0, 0.0, 0.0, 1.5f, 0.0f, lifespan});
    this->addLight(engine, glm::vec3(cam->lookAt.x + (flashLoc.x),cam->lookAt.y + (flashLoc.y),cam->lookAt.z + (flashLoc.z)));
}

int MuzzleFlash::size() {
    return flashes.size();
}

void MuzzleFlash::addLight(GameEngine *engine, glm::vec3 location) {
    Light* addedLight = new Light(LIGHT_TYPE::POINT, 1.0f, 1.0f, 1.0f, 8.0f, location.x, location.y, location.z);
    addedLight->counter = 80;
    engine->addLight(addedLight, false);
    //Duration that light should last
    lights.push_back(addedLight);
}