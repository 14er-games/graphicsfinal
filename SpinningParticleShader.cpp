
#include "SpinningParticleShader.h"

const char* SpinningParticleShader::vertexProgram =
R"FOURTEENER(#version 410 core

layout (location = 0) in vec3 vPos;
layout (location = 1) in float size;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

out float sizeOut;

void main() {
	sizeOut = size;
	gl_Position = viewMatrix * modelMatrix * vec4(vPos, 1);
}
)FOURTEENER";

const char* SpinningParticleShader::geometryProgram =
R"FOURTEENER(#version 410 core

layout(points) in;
uniform mat4 projectionMatrix;
uniform int frameNumber;
in float sizeOut[];

layout(triangle_strip, max_vertices=5) out;
out vec2 texCoord;

// Don't ask, returns in [-1,1]
float rand (vec2 seed) {
	return sin(dot(seed, vec2(8125.3478, 2147.3598)) + 29248634.21247144);
}

void main() {
	float size = sizeOut[0] * 1.5;

	float multConv = 0.05;
	float addConv = 3.1415 / 2.0;
	float state = frameNumber * multConv;

	vec4 center = vec4(gl_in[0].gl_Position.xyz, 1) + 0.2 * vec4(rand(vec2(frameNumber, frameNumber)), rand(vec2(2 * frameNumber, 2 * frameNumber)), rand(vec2(2 * frameNumber, 2 * frameNumber)), 0);

    float cosOff = cos(state); // -1 @ 0
    float sinOff = sin(state);
	gl_Position = projectionMatrix * (center + vec4(size * cosOff, size * sinOff, 0, 0));
    texCoord = vec2(0, 0);
    EmitVertex();

    state += addConv;
    cosOff = cos(state); // -1 @ 0
    sinOff = sin(state);
    gl_Position = projectionMatrix * (center + vec4(size * cosOff, size * sinOff, 0, 0));
    texCoord = vec2(1, 0);
    EmitVertex();

    state += addConv;
    cosOff = cos(state);
    sinOff = sin(state);
	gl_Position = projectionMatrix * (center + vec4(size * cosOff, size * sinOff, 0, 0));
    texCoord = vec2(1, 1);
    EmitVertex();

    state += addConv;
    cosOff = cos(state);
    sinOff = sin(state);
    gl_Position = projectionMatrix * (center + vec4(size * cosOff, size * sinOff, 0, 0));
    texCoord = vec2(0, 1);
    EmitVertex();

    state += addConv;
    cosOff = cos(state);
    sinOff = sin(state);
    gl_Position = projectionMatrix * (center + vec4(size * cosOff, size * sinOff, 0, 0));
    texCoord = vec2(0, 0);
    EmitVertex();

    EndPrimitive();
}
)FOURTEENER";

const char* SpinningParticleShader::fragmentProgram =
R"FOURTEENER(#version 410 core

in vec2 texCoord;
uniform sampler2D tex;

out vec4 fragColorOut;

void main() {
	fragColorOut = texture(tex, texCoord);

	float texA = fragColorOut.a;
	fragColorOut = fragColorOut * (vec4(255, 140, 0, 255) / 255.0);
	fragColorOut.a = texA;

	if (fragColorOut.a < 0.001) {
		discard;
	}
}
)FOURTEENER";

SpinningParticleShader::SpinningParticleShader(std::string texture) {
	maxDirectionalLights = 0;
	maxAmbientLights = 0;
	maxPointLights = 0;

	// Load shader
	GLuint vertex = compileShaderText(SpinningParticleShader::vertexProgram, GL_VERTEX_SHADER);
	GLuint geometry = compileShaderText(SpinningParticleShader::geometryProgram, GL_GEOMETRY_SHADER);
	GLuint fragment = compileShaderText(SpinningParticleShader::fragmentProgram, GL_FRAGMENT_SHADER);

	programHandle = glCreateProgram();

	glAttachShader(programHandle, vertex);
	glAttachShader(programHandle, geometry);
	glAttachShader(programHandle, fragment);

	glLinkProgram(programHandle);

	printLog(programHandle);

	glDetachShader(programHandle, vertex);
	glDetachShader(programHandle, geometry);
	glDetachShader(programHandle, fragment);

	glDeleteShader(vertex);
	glDeleteShader(geometry);
	glDeleteShader(fragment);

	glUseProgram(programHandle);

	// Load texture
	textureId = TextureUtils::loadAndRegisterTexture(texture.c_str());

	// Set up buffers
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);

	// Enable attribs and such
	this->enableAttribs();
	glUniform1i(getUniformLocation("tex"), 0);
}

SpinningParticleShader::~SpinningParticleShader() {
	glDeleteTextures(1, &textureId);
	glBindVertexArray(vao);
	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
}

void SpinningParticleShader::enableAttribs() {
	glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*) 0);
	glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*) (3 * sizeof(GLfloat)));
}

void SpinningParticleShader::drawParticle(float x, float y, float z, float size) {
	glBindVertexArray(vao);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	float data[] = {x, y, z, size};
	glBufferSubData(GL_ARRAY_BUFFER, 0, 4 * sizeof(GLfloat), data);
	glDrawArrays(GL_POINTS, 0, 1);
}
