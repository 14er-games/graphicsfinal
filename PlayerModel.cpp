
#include "engine/shaders/ColorGouradShader.h"
#include "engine/materials/MatteSkyBlue.h"
#include "PlayerModel.h"
#include "engine/scripts/SimpleMovement.h"
#include <CSCI441/objects.hpp>          // draws 3D objects


PlayerModel::PlayerModel() : Object() {
    shaderProgram = new ColorGouradShader();
    material = new MatteSkyBlue();
    setCollider(new Collider(this), false);
    currentHeading = glm::vec3(0.0f,0.0f,-1.0f);
    rotateAngle = 0.0f;
    lastLocation = this->position;
}

void PlayerModel::setCam(Camera* camera) {
    this->cam = camera;
}

void PlayerModel::drawBody(glm::mat4 modelMtx){
    modelMtx = glm::translate(modelMtx, glm::vec3(0.0,-6.0,0.0));
    modelMtx = glm::rotate(modelMtx, camRotation, glm::vec3(0.0,1.0,0.0));
    modelMtx = glm::translate(modelMtx, glm::vec3(0.0,0.0,-2.0));
    modelMtx = glm::scale(modelMtx, glm::vec3(1.5,3,1));
    glUniformMatrix4fv(shaderProgram->getUniformLocation("modelMatrix"), 1, GL_FALSE, &modelMtx[0][0]);
    glm::mat3 normalMtx = glm::mat3( glm::transpose( glm::inverse(modelMtx) ) );
    glUniformMatrix3fv(shaderProgram->getUniformLocation("normalMatrix"), 1, GL_FALSE, &normalMtx[0][0]);
    CSCI441::drawSolidCube(2);
}

void PlayerModel::drawHead(glm::mat4 modelMtx) {
    modelMtx = glm::translate(modelMtx, glm::vec3(0,-10,0));
    modelMtx = glm::rotate(modelMtx, camRotation, glm::vec3(0.0,1.0,0.0));
    modelMtx = glm::translate(modelMtx, glm::vec3(0.0,0.0,-2.0));
    glUniformMatrix4fv(shaderProgram->getUniformLocation("modelMatrix"), 1, GL_FALSE, &modelMtx[0][0]);
    glm::mat3 normalMtx = glm::mat3( glm::transpose( glm::inverse(modelMtx) ) );
    glUniformMatrix3fv(shaderProgram->getUniformLocation("normalMatrix"), 1, GL_FALSE, &normalMtx[0][0]);
    CSCI441::drawSolidSphere(1.5,30,30);
}

void PlayerModel::drawLegs(glm::mat4 modelMtx) {
    //right leg. More modifications to allow for correct rotation
    glm::mat4 rModel = modelMtx;
    rModel = glm::translate(rModel, glm::vec3(0.0,-5,0));
    rModel = glm::rotate(rModel, camRotation, glm::vec3(0.0,1.0,0.0));
    rModel = glm::translate(rModel, glm::vec3(0.0,0.0,-2.0));
    rModel = glm::rotate(rModel, sin(rotateAngle)/2, glm::vec3(1.0,0.0,0.0));
    rModel = glm::translate(rModel, glm::vec3(0.0,5,0));
    rModel = glm::translate(rModel, glm::vec3(0.8,-1,0));
    rModel = glm::scale(rModel, glm::vec3(1.5,5,2));
    glUniformMatrix4fv(shaderProgram->getUniformLocation("modelMatrix"), 1, GL_FALSE, &rModel[0][0]);
    glm::mat3 normalMtx = glm::mat3( glm::transpose( glm::inverse(rModel) ) );
    glUniformMatrix3fv(shaderProgram->getUniformLocation("normalMatrix"), 1, GL_FALSE, &normalMtx[0][0]);
    CSCI441::drawSolidCube(1);

    //left leg. More modifications to allow for correct rotation
    modelMtx = glm::translate(modelMtx, glm::vec3(0.0,-5,0));
    modelMtx = glm::rotate(modelMtx, camRotation, glm::vec3(0.0,1.0,0.0));
    modelMtx = glm::translate(modelMtx, glm::vec3(0.0,0.0,-2.0));
    modelMtx = glm::rotate(modelMtx, -sin(rotateAngle)/2, glm::vec3(1.0,0.0,0.0));
    modelMtx = glm::translate(modelMtx, glm::vec3(0.0,5,0));
    modelMtx = glm::translate(modelMtx, glm::vec3(-0.8,-1,0));
    modelMtx = glm::scale(modelMtx, glm::vec3(1.5,5,2));
    glUniformMatrix4fv(shaderProgram->getUniformLocation("modelMatrix"), 1, GL_FALSE, &modelMtx[0][0]);
    normalMtx = glm::mat3( glm::transpose( glm::inverse(modelMtx) ) );
    glUniformMatrix3fv(shaderProgram->getUniformLocation("normalMatrix"), 1, GL_FALSE, &normalMtx[0][0]);
    CSCI441::drawSolidCube(1);
}

void PlayerModel::draw(){
    shaderProgram->useProgram();
    GameEngine::engineShaderProgram = shaderProgram;

    CSCI441::setVertexAttributeLocations(shaderProgram->getAttributeLocation("vertexPosition"),   // vertex position location
                                         shaderProgram->getAttributeLocation("vertexNormal"),     //vertex normal location
                                         -1);// shaderProgram->getAttributeLocation("vertexTexture"));         //vertex texture location
    // Buffer mvp uniforms
    shaderProgram->uniformMat4("modelMatrix", getModelMatrix());
    shaderProgram->uniformMat4("viewMatrix", GameEngine::engineCamera->getViewMatrix());
    shaderProgram->uniformMat4("projectionMatrix", GameEngine::engineCamera->getProjectionMatrix(GameEngine::engineWindow));

    // Buffer material uniforms
    shaderProgram->uniformVec3("materialColor", glm::vec3(material->r, material->g, material->b));
    shaderProgram->uniformFloat("materialDiffuse", material->diffuse);
    shaderProgram->uniformFloat("materialSpecularity", material->specular);
    // Buffer viewing vector uniform
    shaderProgram->uniformVec3("viewingVector", GameEngine::engineCamera->getViewingVector());

    material->use();


    //printf("%f -> %f \n", this->position.x, lastLocation.x);
    if (InputSystem::isKeyPressed(GLFW_KEY_W) || InputSystem::isKeyPressed(GLFW_KEY_S)){
        rotateAngle += 0.03;
        lastLocation = this->position;
    }else {
        rotateAngle = 0;
    }

    glm::mat4 modelMtx = getModelMatrix();
    //Move to cam offset so that rotation is centered around cam location
    modelMtx = glm::translate(modelMtx, glm::vec3(0.0,0.0,2.0));
    drawBody(modelMtx);
    drawHead(modelMtx);
    drawLegs(modelMtx);


    //Gets rotation for the player by finding angle between current heading and cam heading
    //in the x,z plane
    glm::vec3 newHeading = glm::normalize(cam->lookAt-cam->pos);
    glm::vec2 curr2D = glm::vec2(currentHeading.x, currentHeading.z);
    glm::vec2 new2D = glm::vec2(newHeading.x, newHeading.z);
    float dot = glm::dot(glm::normalize(curr2D), glm::normalize(new2D));
    glm::vec3 axis;
    if (newHeading != currentHeading){
        axis = glm::cross(newHeading, currentHeading);
    }else {
        axis = glm::vec3(0.0,1.0,0.0);
    }
    //Weird floating point issues cause this
    if(dot < 0.99999){
        if (axis.y >0) {
            camRotation += acosf(dot);
        }else {
            camRotation -= acosf(dot);
        }
    }
    currentHeading = newHeading;
}