
#include "TopDownCamera.h"
//Default constructor if there is no object to tie to

TopDownCamera::TopDownCamera(Object *obj) {
    adjustment = glm::vec3(20.0f,0.0,-75.0f);
    this->follow = obj;
    this->camHeight = 100.0f;
    this->pos = follow->position + adjustment + glm::vec3(0.001f, camHeight, 0.001f);
    this->lookAt = follow->position + adjustment;
    this->up = glm::vec3(0.0f, 1.0f, 0.0f);


}

void TopDownCamera::update() {
    this->pos = follow->position + adjustment + glm::vec3(0.001f, camHeight, 0.001f);
    this->lookAt = follow->position + adjustment;

}

void TopDownCamera::setScissoredSize(float width, float height) {
    this->scissorWidth = width;
    this->scissorHeight = height;
}

glm::mat4 TopDownCamera::getProjectionMatrix(GLFWwindow* window) {
    return glm::perspective( 45.0f, (GLfloat) this->scissorWidth / (GLfloat) scissorHeight, 0.001f, 1000.0f );
}

