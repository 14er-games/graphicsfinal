
#include "PlayerPostScript.h"

#include "engine/Object.h"

#include <cstdio>

PlayerPostScript::PlayerPostScript(int* powerupTimer) {
	timerVar = powerupTimer;
}

PlayerPostScript::~PlayerPostScript() {

}

void** PlayerPostScript::run(void** args) {
	// Are we triggered by a powerup?
	for (Object* obj : GameEngine::engine->checkTriggers(GameEngine::currentObject->getCollider())) {
		if (obj->hasTag("powerup")) {
			obj->setDestroy(true);
			*timerVar += 450;

			if (*timerVar > 1200) {
				*timerVar = 1200;
			}
		}

		if (obj->hasTag("enemy")) {
			// Go flying unless have a powerup
			if (!(*timerVar > 0)) {
				GameEngine::currentObject->velocity = glm::vec3(0, 0.5, 0);
			}
		}
	}

	// Don't fall off the world - up or down (or at least respawn)
	if (GameEngine::currentObject->position.y < -120.0f) {
		GameEngine::currentObject->position = glm::vec3(0, 5, 0);
		GameEngine::currentObject->velocity = glm::vec3(0, 0.5, 0);
	}
	if (GameEngine::currentObject->position.y > 120.0f) {
		GameEngine::currentObject->position = glm::vec3(0, 5, 0);
		GameEngine::currentObject->velocity = glm::vec3(0, 0.5, 0);
	}

	return (void**) 0;
}
