Guild Name: Akai Robots

Members: Logan Cooper (hero: Masao Asanaka, email: logancooper@gmail.com)
		Asa Farrer (hero: Doomguy, email: afarrer@mymail.mines.edu
		Isabel Johnson (hero: Ghost of Tsushima, email: isabeljohnson@mymail.mines.edu)
		Patrick James (hero: Pat Man, email: pjjames@mymail.mines.edu)

High level overview: A first person shooter. Attack enemies to defeat them before they launch you into the air. Collect falling powerups to stop the enemies from being able to launch you into the air.

Compiling: All the cpp files need to be added to the cmake project (there's a lot of them), so a cmake recursive glob is probably easiest.
Needs glfw, glew, OpenGL, stdimage, and the class library (for objects) to build. Given how many cpp files there are, it may take a bit of time to compile.

Usage: Controls are printed to the console at start. They are copied here as well:
WASD - Move
Space - Jump
Mouse - Turn the player
Left click - Attack

Bugs: None that we know of in our code.
GLFW full screen windows can be buggy sometimes. If this happens, change line 54 to use 1920 and 1080 instead of -1 and -1 to force a window.

Implementation Details: This was based off of Logan's A7 as the starting point.

Input file format: We opted to not use an input file (even if it was reccomended - it wasn't required and didn't make a ton of sense for our game).

Contribution Distribution:
Logan - (outside of base from my A7), Shader work, powerups
Asa - Player
Patrick - Weapon
Isabel - Enemies
All - Debugging stuff

Time Taken: Probably several hundred combined hours of work across all 4 of us.

Lab Help: This did not seem based on a lab. Course was generally helpful.

Fun: I had fun, 9/10 -Logan, 9/10 - Isabel, 9/10 - Patrick

Easter Eggs: There is an Easter egg achievable by destroying 15 enemies in a row with 100% accuracy.

Rubric stuff (real fast since there's a lot of code):
User interactivity - the whole controls system
Models - Use the class library for the gun and player
Animation - Player has a heirarchical walking animation
Texture programs - Skybox, ground, and the "glichy" particles (and muzzle flash)
Lights - There's a constant directional and ambient light, and dynamic point lights as part of the muzzle flash and when enemies get hit
Materials - Ground, player/gun, powerups, and enemies all use different materials
Shaders - The SpinningParticle shader both has a "glitchy" vertex offset and spins rather than being a static billboard (unique vertex/tess/geometry shader)
The SecondShader applies a better vignette than what we had in the lab, as well as recoloring that changes over time based for showing if you have a powerup (unique fragment shader)
