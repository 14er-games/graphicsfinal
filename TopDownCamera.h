
#ifndef GRAPHICSFINAL_TOPDOWNCAMERA_H
#define GRAPHICSFINAL_TOPDOWNCAMERA_H

#include "engine/Camera.h"

class TopDownCamera : public Camera {
public:
    TopDownCamera(Object* obj);
    void update() override;
    void setScissoredSize(float width, float height);
    glm::mat4 getProjectionMatrix(GLFWwindow* window) ;

private:
    float camHeight;
    glm::vec3 adjustment;
    float scissorWidth = 1920, scissorHeight = 1080;
};

#endif //GRAPHICSFINAL_TOPDOWNCAMERA_H
